<?php

namespace App\Controller;

use App\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/getClients", name="api_clients_get", methods={"GET","HEAD"})
     */
    public function getClientsAction()
    {
        $clients = $this->getDoctrine()->getRepository(Client::class)->findAll();
        return $this->json($clients, 200, ['ContentType' => 'text/json']);
    }
}
