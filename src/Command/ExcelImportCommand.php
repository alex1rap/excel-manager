<?php

namespace App\Command;

use App\Entity\Client;
use PhpOffice\PhpSpreadsheet\Reader\Exception;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ExcelImportCommand extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:excel:import';

    private $container;

    public function __construct(ContainerInterface $container)
    {
        parent::__construct();
        $this->container = $container;
    }

    protected function configure()
    {
        $this->addArgument('filename', InputArgument::REQUIRED, 'Path to excel-document');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $allowedExstensions = ['xls', 'xlsx'];
        $filename = $input->getArgument('filename');
        $realfile = realpath($filename);

        if (!file_exists($realfile)) {
            $output->writeln("File '{$realfile}' does not exists.");
            return -1;
        }

        if (!in_array(pathinfo($realfile)['extension'], $allowedExstensions)) {
            $output->writeln("'{$realfile}' is not an excel-document.");
            return -1;
        }

        try {
            $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReaderForFile($realfile);
            $reader->setReadDataOnly(true);
            $doc = $reader->load($realfile);
            $sheet = $doc->getSheet(0);
        } catch (Exception $e) {
            $output->writeln($e->getMessage());
            return -1;
        }

        $coords = $sheet->getCoordinates();
        $em = $this->container->get('doctrine')->getManager();

        for ($i = 3; $i < count($coords);) {
            [$id, $nick, $birthday] = [
                $sheet->getCell($coords[$i++])->getValue(),
                trim($sheet->getCell($coords[$i++])->getValue()),
                Date::excelToDateTimeObject($sheet->getCell($coords[$i++])->getValue()),
            ];
            $birthDate = $birthday->format('Y-m-d');
            $client = new Client();
            $client->setNick($nick);
            $client->setBirthday($birthday);
            $em->persist($client);
            $output->writeln("Added new client (Nick='{$nick}', Birthday='{$birthDate}')");
        }
        $em->flush();
        $output->writeln('Done.');

        return 0;
    }
}
